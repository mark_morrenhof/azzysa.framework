﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Infostrait.Azzysa.Interfaces;

namespace Infostrait.Azzysa.Connectors
{

    /// <summary>
    /// Abstract base class that supports the basic implementation for invoking a service connector
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class BaseConnector<T> : IDisposable
    {
        // Private members
        private SoapWebServiceInvocation<T> _serviceInvocation;
        
        // Protected members and abstract methods that are implemented by leaf classes
        protected CookieContainer CookieContainer { get; set; }

        /// <summary>
        /// Protected constructor, invoked by leaf class implementation to initialize private members
        /// </summary>
        /// <returns></returns>
        public SoapWebServiceInvocation<T> GetServiceInvocation()
        {
            return _serviceInvocation;
        }

        /// <summary>
        /// Get the name of the web service to connect to
        /// </summary>
        /// <returns></returns>
        protected abstract string GetServiceName();

        /// <summary>
        /// Returns the base Url path to the platform web service page that hosts all web services
        /// </summary>
        /// <returns></returns>
        public abstract string GetServicesUrl();

        /// <summary>
        /// Base class implementation, returns the services Uri retrieved from the settings provider
        /// </summary>
        /// <returns></returns>
        private string GetServiceUrl()
        {
            var servicesUrl = GetServicesUrl();
            string serviceUri;
            if (servicesUrl.EndsWith("/") || servicesUrl.EndsWith("\\"))
            {
                serviceUri = servicesUrl + GetServiceName();
            }
            else
            {
                serviceUri = servicesUrl + "/" + GetServiceName();
            }

            return serviceUri;
        }

        /// <summary>
        /// Returns the operation name that this connector will invoke
        /// </summary>
        /// <returns></returns>
        protected abstract string GetOperationName();

        /// <summary>
        /// Returns the type of web service to invoke, so the invocation object can determine the way of processing the result SOAP response
        /// </summary>
        /// <returns></returns>
        protected abstract SoapWebServiceInvocation<T>.EServiceType GetServiceType();

        /// <summary>
        /// Optional, provided by an implementation to process service invocation result when it is not deserializable by the .NET serializers
        /// </summary>
        protected virtual IServiceInvocationResultProcessor<T> GetResultProcessor()
        {
            return null;
        }

        /// <summary>
        /// Returns the Uri representation of the service page that hosts all web services
        /// </summary>
        /// <returns></returns>
        public Uri GetServicesUri()
        {
            return new Uri(GetServicesUrl());
        }

        /// <summary>
        /// Returns the Uri representation of the connected web service
        /// </summary>
        /// <returns></returns>
        public Uri GetServiceUri()
        {
            return new Uri(GetServiceUrl());
        }

        /// <summary>
        /// Performs the actual invocation of the web service using the specified parameters
        /// </summary>
        /// <param name="serviceParameters">Parameters to provide to the web service invocation</param>
        /// <returns>The result of the web service invocation constructed as an object of type T</returns>
        protected T Invoke(Dictionary<string, object> serviceParameters)
        {

            // Initiate invocation object
            _serviceInvocation = new SoapWebServiceInvocation<T>(GetServiceUrl(), GetServiceName(), GetOperationName(),
                string.Empty, GetServiceType(), GetResultProcessor(), CookieContainer);

            // Connect with the web service and invoke the operation
            // Add parameters to the service
            foreach (var parameter in serviceParameters)
            {
                // Check for a valid value
                if (parameter.Value is string)
                {
                    // Just add parameter value as-is
                    _serviceInvocation.AddParameter(parameter.Key, (string)parameter.Value);
                }
                else if (parameter.Value != null)
                {
                    // Serialize the parameter value
                    var stream = new MemoryStream();

                    // First, try to serialzie the type using the XmlSerializer
                    try
                    {
                        var serializer = new XmlSerializer(parameter.Value.GetType());
                        serializer.Serialize(stream, parameter.Value);
                    }
                    catch (NotSupportedException)
                    {
                        // XmlSerializer doesn't support the provided type to serialized / deserialized, try DataContractSerializer instead
                        var serializer = new DataContractSerializer(typeof(T));
                        serializer.WriteObject(stream, parameter.Value);
                    }

                    // Reset the stream and read contents as string
                    stream.Position = 0;
                    var bytes = new byte[stream.Length];
                    stream.Read(bytes, 0, (int)stream.Length);

                    // Add the parameter
                    _serviceInvocation.AddParameter(parameter.Key, System.Text.Encoding.UTF8.GetString(bytes));
                }
                else
                {
                    // Just add empty parameter value
                    _serviceInvocation.AddParameter(parameter.Key, String.Empty);
                }
            }

            // Perform actual invoke and return the result
            return _serviceInvocation.Invoke();
        }

        /// <summary>
        /// Closes the connector by cleaning internal invocation objects and the cookie container
        /// </summary>
        public void Dispose()
        {
            _serviceInvocation?.Dispose();
            _serviceInvocation = null;
            CookieContainer = null;
        }
    }
}