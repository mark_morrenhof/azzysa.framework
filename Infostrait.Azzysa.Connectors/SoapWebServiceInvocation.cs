﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using Infostrait.Azzysa.Interfaces;

namespace Infostrait.Azzysa.Connectors
{
    public class SoapWebServiceInvocation<T> : IDisposable
    {

        // Private members
        private string Url { get; set; }
        private string Method { get; set; }
        private string SoapAction { get; set; }
        private string OperationNamespace { get; set; }
        private bool EncodeParameters { get; set; }
        private HttpWebRequest _request { get; set; }
        private List<InvocationParameter> _parameters { get; set; }
        private EServiceType _serviceType { get; set; }

        /// <summary>
        /// Used to set the type of SOAP webservice that is connected to
        /// </summary>
        public enum EServiceType
        {
            /// <summary>
            /// Connecting to a .NET webservice
            /// </summary>
            DotNet,

            /// <summary>
            /// Connecting to a 3DExperience/ENOVIA webservice
            /// </summary>
            ENOVIA

        }

        /// <summary>
        /// Implementation set through the constructor by the caller to customize the SOAP response processing when .NET serializers cannot read (deserialize) the result to an object
        /// </summary>
        private IServiceInvocationResultProcessor<T> _ResultProcessor;

        /// <summary>
        /// Cookie container that is passed to the HTTPWebRequest object when invoking the web service
        /// </summary>
        internal CookieContainer _CookieContainer;

        /// <summary>
        /// Private class that supports parameterized SOAP web service invocation
        /// </summary>
        private class InvocationParameter
        {

            public string Name { get; set; }

            public string Value { get; set; }

            public string DateType { private get; set; }

            public string GetTypeAs()
            {
                return "\"" + DateType + "\"";
            }

        }

        /// <summary>
        /// Constructs a new web service invocation
        /// </summary>
        /// <param name="baseUrl">The full service uri to connect to the web service</param>
        /// <param name="soapAction">Name of the web service that is sent as a SOAP action header</param>
        /// <param name="method">The name of the operation to invoke</param>
        /// <param name="operationNamespace">Namespace of the xmlns attribute at the soap envelope declaration</param>
        /// <param name="serviceType"></param>
        /// <param name="ResultProcessor">Optional, provided by an implementation to process service invocation result when it is not deserializable by the .NET serializers</param>
        public SoapWebServiceInvocation(string baseUrl, string soapAction, string method, string operationNamespace, EServiceType serviceType, 
            IServiceInvocationResultProcessor<T> ResultProcessor, CookieContainer CookieContainer)
        {
            // Check parameters
            if (string.IsNullOrWhiteSpace(baseUrl)) throw new ArgumentNullException("baseUrl");
            if (string.IsNullOrWhiteSpace(soapAction)) throw new ArgumentNullException("soapAction");
            if (string.IsNullOrWhiteSpace(method)) throw new ArgumentNullException("method");

            // Set private members
            Url = baseUrl;
            Method = method;
            SoapAction = soapAction;
            OperationNamespace = operationNamespace;
            _serviceType = serviceType;
            _parameters = new List<InvocationParameter>();
            EncodeParameters = true;
            _ResultProcessor = ResultProcessor;
            _CookieContainer = CookieContainer;

        }

        /// <summary>
        /// Returns the HttpWebRequest object that is created and used when invoking the web service
        /// </summary>
        /// <returns></returns>
        public HttpWebRequest GetHttpRequest()
        {
            return _request;
        }

        /// <summary>
        /// Invokes a Web Method, with its parameters encoded or not.
        /// </summary>
        public T Invoke()
        {
            // Set initial SOAP envelope based on the SOAP service type
            string soapStr;
            if (_serviceType == EServiceType.DotNet)
            {
                soapStr = @"<?xml version=""1.0"" encoding=""utf-8""?><soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/""><soap:Body><{0} xmlns=""{2}"">{1}</{0}></soap:Body></soap:Envelope>";
            }
            else if (_serviceType == EServiceType.ENOVIA)
            {
                soapStr =
                    @"<?xml version=""1.0"" encoding=""utf-8""?><soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:soapenc=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:tns=""http://DefaultNamespace"" xmlns:types=""http://DefaultNamespace/encodedTypes"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""><soap:Body soap:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/""><tns:{0} xmlns=""{2}"">{1}</tns:{0}></soap:Body></soap:Envelope>";
            }
            else
            {
                throw new NotImplementedException("Unknown service type: " + _serviceType.ToString());
            }

            // Create the request object
            _request = (HttpWebRequest)WebRequest.Create(Url);
            _request.Headers.Add("SOAPAction", "\"" + SoapAction + "\"");
            _request.ContentType = "text/xml;charset=utf-8";
            _request.Accept = "text/xml";
            _request.Method = "POST";
            _request.Timeout = int.MaxValue;
            _request.CookieContainer = _CookieContainer;

            var postValues = "";
            using (var stm = _request.GetRequestStream())
            {
                foreach (var param in _parameters)
                {
                    if (_serviceType == EServiceType.DotNet)
                    {
                        if (EncodeParameters) postValues += string.Format("<{0}>{1}</{0}>", HttpUtility.HtmlEncode(param.Name), HttpUtility.HtmlEncode(param.Value));
                        else postValues += string.Format("<{0}>{1}</{0}>", param.Name, param.Value);
                    }
                    else if (_serviceType == EServiceType.ENOVIA)
                    {
                        if (EncodeParameters) postValues += string.Format("<{0} xsi:type={2}>{1}</{0}>", HttpUtility.HtmlEncode(param.Name), HttpUtility.HtmlEncode(param.Value), param.GetTypeAs());
                        else postValues += string.Format("<{0} xsi:type={2}>{1}</{0}>", param.Name, param.Value, param.GetTypeAs());
                    }
                }

                // Set operation node
                soapStr = string.Format(soapStr, Method, postValues, OperationNamespace);
                using (var stmw = new StreamWriter(stm))
                {
                    stmw.Write(soapStr);
                    stmw.Flush();
                }
                stm.Flush();
            }

            try
            {
                var response = _request.GetResponse();
                using (var responseReader = new StreamReader(response.GetResponseStream()))
                {
                    var result = HttpUtility.HtmlDecode(responseReader.ReadToEnd());
                    if (_serviceType == EServiceType.ENOVIA)
                    {
                        // Apache TomCat may returned a serialized object, so we have remove the XML declaration in the middle of the message
                        var idx = result.IndexOf("<?xml", 1, StringComparison.Ordinal);
                        while (idx > 1)
                        {
                            result = result.Remove(idx,
                                ((result.IndexOf("?>", idx, StringComparison.Ordinal) - idx) + 2));
                            idx = result.IndexOf("<?xml", 1, StringComparison.Ordinal);
                        }
                    }

                    // Parse the result
                    return ExtractResult(result, (_serviceType == EServiceType.DotNet));
                }
            }

            catch (WebException ex)
            {
                var responseStream = (MemoryStream)ex.Response.GetResponseStream();
                if (responseStream != null)
                {
                    var errorText =
                        System.Text.Encoding.ASCII.GetString(responseStream.GetBuffer());
                    var e = new ApplicationException("Server error while invoking the web service: " +
                        System.Environment.NewLine + errorText);
                    e.Data.Add("postValues", postValues);
                    e.Data.Add("soapStr", soapStr);
                    e.Data.Add("Original Exception details", ex.ToString());

                    throw e;

                }

                // Unknown server error
                throw new ApplicationException("Unknown server error: " +
                                               System.Environment.NewLine + "postValues: " + postValues +
                                               System.Environment.NewLine + "soapStr : " + soapStr +
                                               System.Environment.NewLine + ex.ToString(), ex);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Unexpected error occured invoking SOAP service using the SOAP string: " +
                    System.Environment.NewLine + "postValues: " + postValues +
                    System.Environment.NewLine + "soapStr : " + soapStr +
                    System.Environment.NewLine + ex.ToString(), ex);
            }
        }

        /// <summary>
        /// Adds a parameter to the WebMethod invocation.
        /// </summary>
        /// <param name="name">Name of the WebMethod parameter (case sensitive)</param>
        /// <param name="value">Value to pass to the paramenter</param>
        public void AddParameter(string name, string value)
        {
            _parameters.Add(new InvocationParameter { DateType = "xsd:string", Name = name, Value = value });
        }

        /// <summary>
        /// Adds a parameter to the WebMethod invocation.
        /// </summary>
        /// <param name="name">Name of the WebMethod parameter (case sensitive)</param>
        /// <param name="value">Value to pass to the paramenter</param>
        /// <param name="dataType">SOAP data type that the parameter is sent in (for ENOVIA service type only)</param>
        public void AddParameter(string name, string value, string dataType)
        {
            _parameters.Add(new InvocationParameter { DateType = dataType, Name = name, Value = value });
        }

        /// <summary>
        /// Method that extracts the SOAP response from the SOAP enveloppe and reads the object as the requested type
        /// </summary>
        /// <param name="result">string containing the SOAP response</param>
        /// <param name="unescapeResultString">Boolean to unescape the SOAP response string (by using the <see cref="HttpUtility.HtmlDecode(string)"/> method)</param>
        /// <returns></returns>
        private T ExtractResult(string result, Boolean unescapeResultString)
        {
            // Selects just the elements with namespace http://tempuri.org/ (i.e. ignores SOAP namespace)
            XDocument responseSoap;
            if (unescapeResultString)
            {
                responseSoap = XDocument.Parse(HttpUtility.HtmlDecode(result));
            }
            else
            {
                responseSoap = XDocument.Parse(result);
            }
            
            // Parse the return message
            var namespMan = new XmlNamespaceManager(new NameTable());
            namespMan.AddNamespace("foo", "http://tempuri.org/");
            var xDoc = new XmlDocument();
            using (var xDocReader = responseSoap.CreateReader())
            {
                xDoc.Load(xDocReader);
            }

            // Check the service type, if ENOVIA - a different XML is returned
            XmlNode webMethodResult;
            if (_serviceType == EServiceType.ENOVIA)
            {
                webMethodResult = xDoc.SelectSingleNode("//" + Method + "Return");
            }
            else
            {
                webMethodResult = xDoc.SelectSingleNode("//" + Method + "Response");
            }


            if (webMethodResult == null)
            {
                throw new ApplicationException("Unable to parse web service result: " + Environment.NewLine + result);
            }

            if (webMethodResult.Attributes != null && webMethodResult.Attributes["href"] != null)
            {
                // Contains referenced result element, get this element to parse
                var attr = webMethodResult.Attributes["href"].Value;
                webMethodResult = xDoc.SelectSingleNode("//multiRef[@id='" + attr.Substring(1) + "']");
            }


            if (webMethodResult == null)
            {
                throw new ApplicationException("Unable to parse web service result: " + Environment.NewLine + result);
            }

            // Try to read as XML response, if exception occurs return the value
            string streamContent;
            var doc = new XmlDocument();
            try
            {
                doc.LoadXml(webMethodResult.InnerXml);
            }
            catch (XmlException ex)
            {
                // Not valid XML, use literal text instead
                doc = null;
            }

            if (doc != null)
            {
                var xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", "yes");
                var documentElement = doc.DocumentElement;
                doc.InsertBefore(xmlDeclaration, documentElement);
                if (doc.DocumentElement != null)
                {
                    doc.DocumentElement.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                    doc.DocumentElement.SetAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
                    streamContent = doc.OuterXml;
                }
                else
                {
                    throw new ApplicationException("Unable to parse web service result " + Environment.NewLine + result);
                }
            }
            else
            {
                streamContent = webMethodResult.InnerXml;
            }

            // Check if a custom result processor has been set
            if (_ResultProcessor != null)
            {
                // Use custom processor
                return _ResultProcessor.Process(streamContent);
            }

            // Use .NET serializer to extract the result
            // Try to deserialize using the XmlSerializer first
            var stream = new MemoryStream(System.Text.Encoding.ASCII.GetBytes(streamContent))
            {
                Position = 0
            };
            try
            {
                var serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(stream);
            }
            catch (NotSupportedException)
            {
                // XmlSerializer doesn't support the provided type to serialized / deserialized, try DataContractSerializer instead
                var serializer = new DataContractSerializer(typeof(T));
                return (T)serializer.ReadObject(stream);
            }
        }

        /// <summary>
        /// Remove all xmlns:* instances from the passed XmlDocument to simplify our xpath expressions
        /// </summary>
        private XDocument RemoveNamespaces(XDocument oldXml)
        {
            // FROM: http://social.msdn.microsoft.com/Forums/en-US/bed57335-827a-4731-b6da-a7636ac29f21/xdocument-remove-namespace?forum=linqprojectgeneral
            return XDocument.Parse(Regex.Replace(
                oldXml.ToString(),
                @"(xmlns:?[^=]*=[""][^""]*[""])",
                "",
                RegexOptions.IgnoreCase | RegexOptions.Multiline)
            );
        }

        /// <summary>
        /// Closes the SOAP web service invocation object by removing the reference to the HttpWebRequest object, processors and the cookie container
        /// </summary>
        public void Dispose()
        {
            _request = null;
            _ResultProcessor = null;
            _CookieContainer = null;
            _parameters = null;
        }

    }
}