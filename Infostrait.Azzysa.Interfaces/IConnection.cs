﻿using System;
using System.Net;

namespace Infostrait.Azzysa.Interfaces
{

    /// <summary>
    /// IConnection implementation that supports maintaining a connection with the platform
    /// </summary>
    public interface IConnection : IDisposable
    {

        /// <summary>
        /// Returns the cookie container holding the session cookies
        /// </summary>
        /// <returns></returns>
        CookieContainer GetCookieContainer();
        
        /// <summary>
        /// Returns the initialized web client (applies only to RestApi based connection pool)
        /// </summary>
        /// <returns></returns>
        WebClient GetWebClient();
    }
}