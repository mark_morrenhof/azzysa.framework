﻿using System;
using System.Net;

namespace Infostrait.Azzysa.Interfaces
{

    /// <summary>
    /// IConnectionLock that is returned from <see cref="IConnectionPool.GetPoolObject(string,string,string,string)" />
    /// </summary>
    public interface IConnectionLock : IDisposable
    {

        /// <summary>
        /// Gets or sets the setting, indicating to dispose the <see cref="IConnection"/> object when disposing the current <see cref="IConnectionLock"/>
        /// </summary>
        bool DisposeConnectionOnDispose { get; set; }

        /// <summary>
        /// Returns the connected IConnection object
        /// </summary>
        /// <returns></returns>
        IConnection GetConnection();

        /// <summary>
        /// Returns the cookie container holding the session cookies
        /// </summary>
        /// <returns></returns>
        CookieContainer GetCookieContainer();

        /// <summary>
        ///  Returns the session id
        /// </summary>
        /// <returns></returns>
        string GetSessionId();

        /// <summary>
        /// Unlocks the current IConnection making this IConnectionLock unavailable
        /// </summary>
        void Unlock();

    }
}