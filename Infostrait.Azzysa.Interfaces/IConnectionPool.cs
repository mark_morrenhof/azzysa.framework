﻿using System;

namespace Infostrait.Azzysa.Interfaces
{

    /// <summary>
    /// Implementation that hosts all connections with the platform, use this pool to create and lock new IConnection objects
    /// </summary>
    public interface IConnectionPool : IDisposable
    {

        /// <summary>
        /// Returns the number of initialized pool objects
        /// </summary>
        /// <returns></returns>
        int GetPoolCount();

        /// <summary>
        /// Gets the number of pool objects available or not (depending on the IsAvailable parameter)
        /// </summary>
        /// <param name="isAvailable">True to return the number of available objects, false to return the number of in used objects</param>
        /// <returns></returns>
        int GetAvailableCount(bool isAvailable);

        /// <summary>
        /// Returns an IConnectionLock object that is available from the internal pool, or create a new one if not available and lock it exclusively to the caller
        /// </summary>
        /// <param name="servicesUrl">Base url to web services, login service URL is constructed based on this url</param>
        /// <param name="userName">User name to connect with (is checked after getting an existing connection from the pool)</param>
        /// <param name="password">Password for the specified user</param>
        /// <param name="vault">Name of the vault to specify on the login procedure</param>
        /// <returns></returns>
        IConnectionLock GetPoolObject(string servicesUrl, string userName, string password, string vault);
        
    }
}