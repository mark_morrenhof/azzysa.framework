﻿using Infostrait.Azzysa.EF.Interfaces;

namespace Infostrait.Azzysa.Interfaces
{
    public interface IExchangeFrameworkProvider
    {

        /// <summary>
        /// Invokes the Exchange Framework web service and returns the returned (processed) package
        /// </summary>
        /// <param name="package">Data package holding the objects to process</param>
        /// <param name="configurationName">Name of the ENOVIA page object that holds the configuration</param>
        /// <returns></returns>
        IDataPackageBase ProcessPackage(IDataPackageBase package, string configurationName);

    }
}