﻿using System;
using System.Net;

namespace Infostrait.Azzysa.Interfaces
{

    /// <summary>
    /// IRuntime that exposes the azzysa runtime methods
    /// </summary>
    public interface IRuntime : IDisposable
    {
        
        /// <summary>
        /// Returns the shared cookie container for web service invocations
        /// </summary>
        /// <remarks>In case of IRuntime implementation that supports pooling, this property may return null in case of pooled CookieContainers</remarks>
        CookieContainer CookieContainer { get; } 

        /// <summary>
        /// Returns the <see cref="ISettingsProvider"/> implementation that provides the settings to the IRuntime implementation
        /// </summary>
        ISettingsProvider SettingsProvider { get; }

        /// <summary>
        /// Returns the <see cref="IExchangeFrameworkProvider"/> implementation that is applicable to the IRuntime implementation
        /// </summary>
        /// <param name="connectionLock"><see cref="IConnectionLock"/> implementation that holds the appropriate CookieContainer required to establish a connection</param>
        /// <returns></returns>
        IExchangeFrameworkProvider GetExchangeFrameworkProvider(IConnectionLock connectionLock);

        /// <summary>
        /// Returns the <see cref="Uri"/> representation of the service page that hosts all web services
        /// </summary>
        Uri ServicesUri { get; }

        /// <summary>
        /// Returns the base Url path to the platform web service page that hosts all web services
        /// </summary>
        string ServicesUrl { get; }

        /// <summary>
        /// Returns whether the path to the web services is going through an secured (Https) connection
        /// </summary>
        bool IsSecuredServicesConnection { get; }

        /// <summary>
        /// Resets internal cookie container (if applicable)
        /// </summary>
        void ResetCookieContainer();

        /// <summary>
        /// Returns the azzysa framework version
        /// </summary>
        string Version { get; }

        /// <summary>
        /// Returns the <see cref="IConnectionPool"/> implementation, if set by the runtime
        /// </summary>
        IConnectionPool ConnectionPool { get; }

        /// <summary>
        /// Initializes the IRuntime implementation (e.g. during Application Startup)
        /// </summary>
        void Initialize();

        /// <summary>
        /// Gets or sets the site name for this IRuntime
        /// </summary>
        string SiteName { get; set; }

        /// <summary>
        /// Gets or sets the server name for this IRuntime
        /// </summary>
        string ServerName { get; set; }

        /// <summary>
        /// Gets or sets the root path of the running IRuntime implementation
        /// </summary>
        string RootPath { get; set; }

        /// <summary>
        /// Gets or sets the configured user name in order to initialize a <see cref="IConnection"/> object
        /// </summary>
        string UserName { get; set; }

        /// <summary>
        /// Gets or sets the configured password in order to initialize a <see cref="IConnection"/> object
        /// </summary>
        string Password { get; set; }

        /// <summary>
        /// Gets or sets the configured vault in order to initialize a <see cref="IConnection"/> object
        /// </summary>
        string Vault { get; set; }

    }
}