﻿namespace Infostrait.Azzysa.Interfaces
{

    /// <summary>
    /// Represents a runtime value, indicating whether the current value is the preconfigured value and holds the current value
    /// </summary>
    public interface IRuntimeSettingValue
    {
        /// <summary>
        /// Gets or sets the current value
        /// </summary>
        string Value { get; set; }

        /// <summary>
        /// Indicating whether the current value is the configured value or has been set after initializing
        /// </summary>
        bool IsConfigured { get; set; }

    }
}