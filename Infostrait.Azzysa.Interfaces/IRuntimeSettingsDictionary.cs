﻿using System.Collections.Generic;

namespace Infostrait.Azzysa.Interfaces
{

    /// <summary>
    /// Holds the collection of runtime values
    /// </summary>
    public interface IRuntimeSettingsDictionary : IDictionary<string, IRuntimeSettingValue>
    {
         
    }

}