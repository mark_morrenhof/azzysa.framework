﻿namespace Infostrait.Azzysa.Interfaces
{

    /// <summary>
    /// Implementation of this that supports the SOAP web service invocation translate the return value of the web service invocation into the requested type T
    /// </summary>
    /// <typeparam name="T">Generic type returned by the SOAP web service</typeparam>
    public interface IServiceInvocationResultProcessor<T>
    {

        /// <summary>
        /// Performs the actual transforming of the return value to the requested type
        /// </summary>
        /// <param name="input">Response from the SOAP web service</param>
        /// <returns></returns>
        T Process(string input);

    }
}