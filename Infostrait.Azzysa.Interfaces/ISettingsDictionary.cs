﻿using System.Collections.Generic;

namespace Infostrait.Azzysa.Interfaces
{

    /// <summary>
    /// Holds the collection of settings provided to and by the runtime
    /// </summary>
    public interface ISettingsDictionary : IDictionary<string, string>
    {
         
    }
}