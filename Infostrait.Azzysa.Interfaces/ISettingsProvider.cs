﻿using System.Collections.Generic;

namespace Infostrait.Azzysa.Interfaces
{

    /// <summary>
    /// ISettingsProvider implementation provides the processing and retrieving of the settings required by the azzysa platform
    /// </summary>
    public interface ISettingsProvider
    {
        /// <summary>
        /// Invoked by the azzysa platform runtime to initialize the settings during the startup of the runtime
        /// </summary>
        /// <param name="runtime">Reference to the <see cref="IRuntime"/> object that is hosting this ISettingsProvider</param>
        /// <param name="connectionString">Connect information provided to the implementation to be able to read the settings</param>
        void Initialize(IRuntime runtime, string connectionString);

        /// <summary>
        /// Returns the connection string that is used to connect to the settings source
        /// </summary>
        /// <returns></returns>
        string GetConnectionString();

        /// <summary>
        /// Get the value of a setting by its name
        /// </summary>
        /// <param name="name">Name of the setting to retrieve the value for</param>
        /// <returns></returns>
        string GetSettingValue(string name);

        /// <summary>
        /// Get the values of a setting by its name
        /// </summary>
        /// <param name="name">Name of the setting to retrieve the values for</param>
        /// <returns></returns>
        IEnumerable<string> GetSettingValues(string name);

        /// <summary>
        /// Returns the collection of currently available settings in the runtime
        /// </summary>
        /// <returns></returns>
        IRuntimeSettingsDictionary GetRuntimeSettings();

        /// <summary>
        /// Reload settings from the provider
        /// </summary>
        /// <param name="connectionString">Connect information provided to the implementation to be able to read the settings</param>
        void ReloadSettings(string connectionString);

        /// <summary>
        /// Updates the provider with new settings
        /// </summary>
        /// <param name="connectionString">Connect information provided to the implementation to be able to update the settings</param>
        /// <param name="newSettings">ISettingsDictionary hold the setting with their values to update</param>
        void UpdateSettings(string connectionString, ISettingsDictionary newSettings);

        /// <summary>
        /// Invoke to update the setting provider with plugin settings
        /// </summary>
        /// <param name="connectionString">Connect information provided to the implementation to be able to read the settings</param>
        /// <param name="newSettings">Dictionary holding all the default settings from the plugin that provides these settings</param>
        void InitializePluginSettings(string connectionString, ISettingsDictionary newSettings);

        /// <summary>
        /// Reads all settings from its source
        /// </summary>
        /// <param name="runtime">Reference to the <see cref="IRuntime"/> object that is hosting this ISettingsProvider</param>
        /// <param name="connectionString">Connect information provided to the implementation to be able to read the settings</param>
        /// <returns></returns>
        ISettingsDictionary ReadSettings(IRuntime runtime, string connectionString);

        /// <summary>
        /// Writes specified settings to its source
        /// </summary>
        /// <param name="runtime">Reference to the <see cref="IRuntime"/> object that is hosting this ISettingsProvider</param>
        /// <param name="connectionString">Connect information provided to the implementation to be able to read the settings</param>
        /// <param name="settings">Settings to write</param>
        void SaveSettings(IRuntime runtime, string connectionString, ISettingsDictionary settings);

    }
}