﻿using System;
using System.IO;
using System.Runtime.Serialization;
using Infostrait.Azzysa.Interfaces;

namespace Infostrait.Azzysa.Providers
{

    /// <summary>
    /// Implementation of the <see cref="SettingsProviderBase"/> base class. Supports XML-file based settings provider
    /// </summary>
    public class DefaultSettingProvider : SettingsProviderBase
    {

        /// <summary>
        /// Implemented by leaf class, reads all settings from the XML file specified by the connection string parameter
        /// </summary>
        /// <param name="runtime">Reference to the <see cref="IRuntime"/> object that is hosting this ISettingsProvider</param>
        /// <param name="connectionString">Full path to the XML file holding the configuration</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">When the connection string is null or empty</exception>
        public override ISettingsDictionary ReadSettings(IRuntime runtime, string connectionString)
        {
            // Check parameters
            if (string.IsNullOrWhiteSpace(connectionString)) throw new ArgumentNullException(nameof(connectionString));
            if (!File.Exists(connectionString)) throw new FileNotFoundException(connectionString);

            // Deserialize the file
            var fs = new FileStream(connectionString, FileMode.Open, FileAccess.Read);
            var ser = new DataContractSerializer(typeof(SettingsDictionary));

            // Read the file
            try
            {
                return (SettingsDictionary) ser.ReadObject(fs);
            }
            finally
            {
                fs.Dispose();
            }
        }

        /// <summary>
        /// Implemented by leaf class, writes specified settings to the XML file specified by the connection string parameter
        /// </summary>
        /// <param name="runtime">Reference to the <see cref="IRuntime"/> object that is hosting this ISettingsProvider</param>
        /// <param name="connectionString">Full path to the XML file holding the configuration</param>
        /// <param name="settings">Settings to write</param>
        /// <exception cref="ArgumentNullException">When the connection string is null or empty</exception>
        public override void SaveSettings(IRuntime runtime, string connectionString, ISettingsDictionary settings)
        {
            if (string.IsNullOrWhiteSpace(connectionString)) throw new ArgumentNullException(nameof(connectionString));

            // Deserialize the file
            var fs = new FileStream(connectionString, FileMode.Create, FileAccess.ReadWrite);
            var ser = new DataContractSerializer(typeof(SettingsDictionary));

            // Write the file
            try
            {
                ser.WriteObject(fs, settings);
                fs.Flush();
            }
            finally
            {
                fs.Dispose();
            }
        }

    }
}