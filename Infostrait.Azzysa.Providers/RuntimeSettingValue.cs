﻿using Infostrait.Azzysa.Interfaces;

namespace Infostrait.Azzysa.Providers
{
    public class RuntimeSettingValue : IRuntimeSettingValue
    {
        /// <summary>
        /// Gets or sets the current value
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Indicating whether the current value is the configured value or has been set after initializing
        /// </summary>
        public bool IsConfigured { get; set; }
    }
}