﻿using System.Collections.Generic;
using Infostrait.Azzysa.Interfaces;

namespace Infostrait.Azzysa.Providers
{

    /// <summary>
    /// Holds the collection of runtime values
    /// </summary>
    public class RuntimeSettingsDictionary : Dictionary<string, IRuntimeSettingValue>, IRuntimeSettingsDictionary
    {
         
    }
}