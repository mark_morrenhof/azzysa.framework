﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Infostrait.Azzysa.Interfaces;

namespace Infostrait.Azzysa.Providers
{

    /// <summary>
    /// Holds the collection of settings provided to and by the runtime
    /// </summary>
    [CollectionDataContract(Name = "Settings", ItemName = "Setting", KeyName = "Key", ValueName = "Value", Namespace = SettingsXmlNamespace)]
    public class SettingsDictionary : Dictionary<string, string>, ISettingsDictionary
    {
        private const string SettingsXmlNamespace = "http://www.infostrait.com/azzysa/settings/20121029";
    }
}