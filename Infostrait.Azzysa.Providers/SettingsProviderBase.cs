﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Infostrait.Azzysa.Interfaces;

namespace Infostrait.Azzysa.Providers
{

    /// <summary>
    /// ISettingsProvider implementation that provides the processing and retrieving of the settings required by the azzysa platform
    /// </summary>
    public abstract class SettingsProviderBase : ISettingsProvider
    {
        // Private members
        private readonly SettingsDictionary _defaultSettings;
        private readonly SettingsDictionary _configuredSettings;
        private IRuntime _runtime;
        private string _connectionString;
        
        /// <summary>
        /// Protected constructor, invoked by leaf class constructor
        /// Initializes private members
        /// </summary>
        protected SettingsProviderBase()
        {
            _defaultSettings = new SettingsDictionary();
            _configuredSettings = new SettingsDictionary();
        }

        /// <summary>
        /// Invoked by the azzysa platform runtime to initialize the settings during the startup of the runtime
        /// </summary>
        /// <param name="runtime">Reference to the <see cref="IRuntime"/> object that is hosting this ISettingsProvider</param>
        /// <param name="connectionString">Connect information provided to the implementation to be able to read the settings</param>
        public void Initialize(IRuntime runtime, string connectionString)
        {
            // Store reference to the runtime
            _runtime = runtime;

            // Store connection string as well
            _connectionString = connectionString;

            _defaultSettings.Add("Infostrait.Azzysa.ProductName", "azzysa");

            _defaultSettings.Add("Infostrait.Azzysa.Environment", "EV6");

            _defaultSettings.Add("Infostrait.Azzysa.Web.EnableVueThumbnails", "false");
            _defaultSettings.Add("Infostrait.Azzysa.Web.SupportedViewers", "Local");
            _defaultSettings.Add("Infostrait.Azzysa.Web.VueAppletConnections", "socket://localhost:5099");
            _defaultSettings.Add("Infostrait.Azzysa.Web.ViewerDownloadLocation", "http://localhost");
            _defaultSettings.Add("Infostrait.Azzysa.Web.jVueVersion", "20.2.2");

            _defaultSettings.Add("Infostrait.Azzysa.Web.TrackingID", "");
            _defaultSettings.Add("Infostrait.Azzysa.Web.AppLoader.EnableDynamicInstall", "true");
            _defaultSettings.Add("Infostrait.Azzysa.Web.UsePersistentCookie", "true");
            _defaultSettings.Add("Infostrait.Azzysa.Web.ExpireCookieInMinutes", "44640");

            _defaultSettings.Add("Infostrait.Azzysa.ServiceConnector.ServicesUri", "http://localhost/enovia/services");
            _defaultSettings.Add("Infostrait.Azzysa.ServiceConnector.Vue.ServiceUri",
                "http://localhost:7001/AutoVueWS/VueBeanWS");
            _defaultSettings.Add("Infostrait.Azzysa.ServiceConnector.Batch.ServiceUri", "http://localhost:4378/bks");

            _defaultSettings.Add("Infostrait.Azzysa.Batch.Queue1", ".\\Private$\\azzysa-batch-1");
            _defaultSettings.Add("Infostrait.Azzysa.Batch.DeadLetterQueue", ".\\Private$\\azzysa.deadletter");


            _defaultSettings.Add("Infostrait.Azzysa.Batch.TimeOut", "600");
            _defaultSettings.Add("Infostrait.Azzysa.Batch.QueueConnector.Handler",
                "Infostrait.Azzysa.Batch.QueueConnector.MirosoftMessageQueueImplementation");
            _defaultSettings.Add("Infostrait.Azzysa.Batch.QueueConnector.Assembly",
                "Infostrait.Azzysa.Batch.QueueConnector");
            _defaultSettings.Add("Infostrait.Azzysa.Batch.StopOnError", bool.FalseString);

            _defaultSettings.Add("Infostrait.Azzysa.SmtpHost", "");
            _defaultSettings.Add("Infostrait.Azzysa.DefaultEmailDomain", "infostrait.nl");

            _defaultSettings.Add("Infostrait.Azzysa.Web.Uri", "http://localhost/Infostrait.Azzysa.Web");

            _defaultSettings.Add("Infostrait.Azzysa.Web.Css", "");

            _defaultSettings.Add("Infostrait.Azzysa.Web.Apps.Search.PredefinedSearches", "Azzysa");
            _defaultSettings.Add("Infostrait.Azzysa.Web.Apps.Search.QueryWhere", "(name ~~ \"*{0}*\") {1} and 'format.file' MATCH '*.?*'");
            _defaultSettings.Add("Infostrait.Azzysa.Web.Apps.Search.QuerySelect",
                "type,name,revision,description,owner,current");
            _defaultSettings.Add("Infostrait.Azzysa.Web.Apps.Search.QueryRootType", "DOCUMENTS");
            _defaultSettings.Add("Infostrait.Azzysa.Web.Apps.Search.Hint", "Searches in field: Name.");
            _defaultSettings.Add("Infostrait.Azzysa.Web.Apps.Search.LinkedReports", "");
            _defaultSettings.Add("Infostrait.Azzysa.Web.Apps.ObjectDetails.RedirectToSearch", bool.FalseString);
            _defaultSettings.Add("Infostrait.Azzysa.AdminAccounts",
                string.Format("admin{0}joe{0}Test Everything{0}pdmwadmin{0}Admin{0}admin@azzysa.com", Environment.NewLine));
            _defaultSettings.Add("Infostrait.Azzysa.Log.Handler", "Infostrait.Azzysa.Log.EventLogHandler");
            _defaultSettings.Add("Infostrait.Azzysa.Web.EnabledApps", "*");

            // Reload the initialized values with the current values from the provider
            ReloadSettings(connectionString);

        }

        /// <summary>
        /// Returns the connection string that is used to connect to the settings source
        /// </summary>
        /// <returns></returns>
        public string GetConnectionString()
        {
            return _connectionString;
        }

        /// <summary>
        /// Get the value of a setting by its name
        /// </summary>
        /// <param name="name">Name of the setting to retrieve the value for</param>
        /// <returns></returns>
        /// <exception cref="ConfigurationErrorsException">When setting by name is not in the settings collection</exception>
        public string GetSettingValue(string name)
        {
            // Check the configured collection
            if (_configuredSettings.ContainsKey(name)) return _configuredSettings[name];

            // Check the default collection
            if (_defaultSettings.ContainsKey(name)) return _defaultSettings[name];

            // Setting not found
            throw new ConfigurationErrorsException(name);
        }

        /// <summary>
        /// Get the values of a setting by its name
        /// </summary>
        /// <param name="name">Name of the setting to retrieve the values for</param>
        /// <returns></returns>
        /// <exception cref="ConfigurationErrorsException">When setting by name is not in the settings collection</exception>
        public IEnumerable<string> GetSettingValues(string name)
        {
            // Iterate through values split by Environment.Newline
            return GetSettingValue(name).Split(new[] {Convert.ToChar(Environment.NewLine)}, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        /// <summary>
        /// Returns the collection of currently available settings in the runtime
        /// </summary>
        /// <returns></returns>
        public IRuntimeSettingsDictionary GetRuntimeSettings()
        {
            var result = new RuntimeSettingsDictionary();

            foreach (var setting in _defaultSettings)
            {
                result.Add(setting.Key, new RuntimeSettingValue() {IsConfigured = _configuredSettings.ContainsKey(setting.Key), Value = setting.Value});
            }

            // Finished
            return result;
        }

        /// <summary>
        /// Reload settings from the provider
        /// </summary>
        /// <param name="connectionString">Connect information provided to the implementation to be able to read the settings</param>
        public void ReloadSettings(string connectionString)
        {
            SetConfiguredValues(ReadSettings(_runtime, connectionString));
        }

        /// <summary>
        /// Updates the provider with new settings
        /// </summary>
        /// <param name="connectionString">Connect information provided to the implementation to be able to read the settings</param>
        /// <param name="newSettings">ISettingsDictionary hold the setting with their values to update</param>
        public void UpdateSettings(string connectionString, ISettingsDictionary newSettings)
        {
            // Update configured collection
            foreach (var setting in newSettings)
            {
                SetConfiguredValue(setting.Key, setting.Value);
            }

            // Save the settings by invoking the leaf class
            SaveSettings(_runtime, connectionString, _configuredSettings);
        }

        /// <summary>
        /// Invoke to update the setting provider with plugin settings
        /// </summary>
        /// <param name="connectionString">Connect information provided to the implementation to be able to read the settings</param>
        /// <param name="newSettings">Dictionary holding all the default settings from the plugin that provides these settings</param>
        public void InitializePluginSettings(string connectionString, ISettingsDictionary newSettings)
        {
            // Check newSettings parameter
            if (newSettings == null || !newSettings.Any()) return;

            // Add settings to the default collection in order to be able to save/update and set configured values
            foreach (var setting in newSettings.Where(setting => !_defaultSettings.ContainsKey(setting.Key)))
            {
                // Update default collection
                _defaultSettings.Add(setting.Key, setting.Value);
            }

            // Reload latest setting information
            ReloadSettings(connectionString);
        }

        /// <summary>
        /// Update the internal configured settings collection with the new value
        /// </summary>
        /// <param name="name">Name of the setting to update</param>
        /// <param name="value">New value of the this setting</param>
        /// <exception cref="ArgumentNullException">When the name parameter does not contain any value (null or empty)</exception>
        /// <exception cref="IndexOutOfRangeException">When the setting is not in the default collection</exception>
        protected void SetConfiguredValue(string name, string value)
        {
            // Check parameter
            if (string.IsNullOrWhiteSpace(name)) throw new ArgumentNullException(nameof(name));
            if (!_defaultSettings.ContainsKey(name))
            {
                _defaultSettings.Add(name, value);
            }
            
            // Add or update the configured value collection
            if (_configuredSettings.ContainsKey(name))
            {
                _configuredSettings[name] = value;
            }
            else
            {
                _configuredSettings.Add(name, value);
            }
        }

        /// <summary>
        /// Update the internal configured settings collection with the new settings
        /// </summary>
        /// <param name="settings">ISettingsDictionary hold the setting with their values to update</param>
        protected void SetConfiguredValues(ISettingsDictionary settings)
        {
            // Check parameter
            if (settings == null) return;

            // Update the settings
            foreach (var setting in settings)
            {
                SetConfiguredValue(setting.Key, setting.Value);
            }
        }

        /// <summary>
        /// Implemented by leaf class, reads all settings from its source
        /// </summary>
        /// <param name="runtime">Reference to the <see cref="IRuntime"/> object that is hosting this ISettingsProvider</param>
        /// <param name="connectionString">Connect information provided to the implementation to be able to read the settings</param>
        /// <returns></returns>
        public abstract ISettingsDictionary ReadSettings(IRuntime runtime, string connectionString);

        /// <summary>
        /// Implemented by leaf class, writes specified settings to its source
        /// </summary>
        /// <param name="runtime">Reference to the <see cref="IRuntime"/> object that is hosting this ISettingsProvider</param>
        /// <param name="connectionString">Connect information provided to the implementation to be able to read the settings</param>
        /// <param name="settings">Settings to write</param>
        public abstract void SaveSettings(IRuntime runtime, string connectionString, ISettingsDictionary settings);

    }
}