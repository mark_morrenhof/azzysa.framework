﻿using System;
using System.Configuration;
using System.Net;
using Infostrait.Azzysa.Interfaces;
using Infostrait.Azzysa.Providers;

namespace Infostrait.Azzysa.Runtime
{
    public abstract class BaseRuntime : IRuntime
    {
        
        // Runtime settings names, configurable by the application.config file
        public const string RootpathSetting = "Infostrait.Azzysa.Runtime.RootPath";
        public const string SettingProviderAssemblySetting = "Infostrait.Azzysa.Runtime.SettingsProvider.Assembly";
        public const string SettingProviderHandlerSetting = "Infostrait.Azzysa.Runtime.SettingsProvider.Handler";
        public const string SettingProviderConnectionSetting = "Infostrait.Azzysa.Runtime.SettingsProvider.ConnectionString";
        public const string ServerNameSetting = "Infostrait.Azzysa.Runtime.ServerName";
        public const string SiteNameSetting = "Infostrait.Azzysa.Runtime.Site";
        protected const string UserNameSetting = "Infostrait.Azzysa.ConnectionPool.UserName";
        protected const string PasswordSetting = "Infostrait.Azzysa.ConnectionPool.Password";
        protected const string VaultSetting = "Infostrait.Azzysa.ConnectionPool.Vault";

        /// <summary>
        /// Default constructor, initializes private members
        /// </summary>
        /// <param name="settingsProvider">Provide implementation to override base class implementation of initializing the <see cref="ISettingsProvider"/> object</param>
        /// <param name="connectionString"></param>
        /// <param name="serverName">Set the name of the azzysa server</param>
        /// <param name="siteName">Set the name of the site (where this server is member of)</param>
        /// <param name="rootPath">Sets the azzysa platform root directory (used for determine persistent storage, temp storary and application contents)</param>
        /// <exception cref="ArgumentNullException">When nog settings provider is provided</exception>
        protected BaseRuntime(ISettingsProvider settingsProvider, string connectionString, string serverName, string siteName, string rootPath)
        {
            // Set the settings provider
            if (settingsProvider == null) { throw new ArgumentNullException(nameof(settingsProvider));}
            SettingsProvider = settingsProvider;

            // In order to support the settings provider (if dependend on a web service, set the ServiceUrl and ServiceUri property on the runtime
            ServicesUrl = connectionString;
            ServicesUri = new Uri(ServicesUrl);

            // Store server and site name in the runtime
            ServerName = serverName;
            SiteName = siteName;
            RootPath = rootPath;
        }
        
        /// <summary>
        /// Returns the <see cref="ISettingsProvider"/> implementation that provides the settings to the IRuntime implementation. 
        /// The settings provider to load, is configured through the application configuration (app.config / web.config) file
        /// </summary>
        public ISettingsProvider SettingsProvider { get; }

        /// <summary>
        /// Gets or sets the site name for this IRuntime
        /// </summary>
        public string SiteName { get; set; }

        /// <summary>
        /// Gets or sets the server name for this IRuntime
        /// </summary>
        public string ServerName { get; set; }

        /// <summary>
        /// Gets or sets the root path of the running IRuntime implementation
        /// </summary>
        public string RootPath { get; set; }

        public string UserName { get; set; }
        public string Password { get; set; }
        public string Vault { get; set; }
        
        /// <summary>
        /// Returns the <see cref="IExchangeFrameworkProvider"/> implementation that is applicable to the IRuntime implementation
        /// </summary>
        /// <param name="connectionLock"><see cref="IConnectionLock"/> implementation that holds the appropriate CookieContainer required to establish a connection</param>
        /// <returns></returns>
        public abstract IExchangeFrameworkProvider GetExchangeFrameworkProvider(IConnectionLock connectionLock);

        /// <summary>
        /// Returns the <see cref="Uri"/> representation of the service page that hosts all web services
        /// </summary>
        public Uri ServicesUri { get; set; }

        /// <summary>
        /// Returns the base Url path to the platform web service page that hosts all web services
        /// </summary>
        public string ServicesUrl { get; set; }

        /// <summary>
        /// Returns whether the path to the web services is going through an secured (Https) connection
        /// </summary>
        public bool IsSecuredServicesConnection => ServicesUri.Scheme.ToLower() == "https";

        /// <summary>
        /// Returns the azzysa framework version
        /// </summary>
        public string Version => GetType().Assembly.GetName().Version.ToString();

        // Abstracts

        /// <summary>
        /// Returns the shared cookie container for web service invocations
        /// </summary>
        /// <remarks>In case of IRuntime implementation that supports pooling, this property may return null in case of pooled CookieContainers</remarks>
        public abstract CookieContainer CookieContainer { get; }

        /// <summary>
        /// Resets internal cookie container (if applicable)
        /// </summary>
        public abstract void ResetCookieContainer();

        /// <summary>
        /// Returns the <see cref="IConnectionPool"/> implementation, if set by the runtime
        /// </summary>
        public abstract IConnectionPool ConnectionPool { get; }

        /// <summary>
        /// Initializes the BaseRuntime, leaf class implementation (e.g. during Application Startup)
        /// </summary>
        /// <remarks>Always invoke base class implementation in leaf class</remarks>
        public virtual void Initialize()
        {
            // Initialize the settings provider
            SettingsProvider.Initialize(this, ServicesUrl);

            // Get services url and Uri (with the latest information from the settings provider)
            ServicesUrl = SettingsProvider.GetSettingValue("Infostrait.Azzysa.ServiceConnector.ServicesUri");
            ServicesUri = new Uri(ServicesUrl);
        }

        /// <summary>
        /// Implemented by leaf class, invoked by BaseRuntime when disposing
        /// </summary>
        public abstract void Dispose();

    }
}