﻿using System;
using System.Net;
using Infostrait.Azzysa.Interfaces;

namespace Infostrait.Azzysa.Runtime
{
    public abstract class PoolRuntime : BaseRuntime
    {
        // Private members
        private IConnectionPool _connectionPool;

        /// <summary>
        /// Default constructor, initializes private members and <see cref="IConnectionPool"/> object
        /// </summary>
        /// <param name="settingsProvider">Provide implementation to override base class implementation of initializing the <see cref="ISettingsProvider"/> object</param>
        /// <param name="connectionString"></param>
        /// <param name="serverName">Set the name of the azzysa server</param>
        /// <param name="siteName">Set the name of the site (where this server is member of)</param>
        /// <param name="rootPath">Sets the azzysa platform root directory (used for determine persistent storage, temp storary and application contents)</param>
        /// <exception cref="ArgumentNullException">When nog settings provider is provided</exception>
        protected PoolRuntime(ISettingsProvider settingsProvider, string connectionString, string serverName, string siteName, string rootPath) : base(settingsProvider, connectionString, serverName, siteName, rootPath)
        {
        }

        /// <summary>
        /// Returns the initialized connection pool
        /// </summary>
        public override IConnectionPool ConnectionPool => _connectionPool;

        /// <summary>
        /// Initializes the connection pool object
        /// </summary>
        /// <exception cref="ApplicationException">When the (by leaf class ) member <see cref="InitializeConnectionPool"/> returns null</exception>
        public override void Initialize()
        {
            // Invoke base implementation
            base.Initialize();

            // Initialize the connection pool
            _connectionPool = InitializeConnectionPool();

            // Check the connection pool
            if (_connectionPool == null) throw new ApplicationException(nameof(ConnectionPool));
        }

        /// <summary>
        /// Not one cookie container for pooled runtime, use <see cref="IConnectionPool"/> to obtain connection lock through <see cref="IConnectionPool.GetPoolObject(string,string,string,string)"/> to obtain valid CookieContainer
        /// </summary>
        public sealed override CookieContainer CookieContainer => null;

        /// <summary>
        /// Not one cookie container for pooled runtime, resetting of the cookie container is handled by the derived <see cref="IConnectionPool"/> implementation
        /// </summary>
        public sealed override void ResetCookieContainer()
        {
            // Do nothing...
        }

        /// <summary>
        /// Implemented by leaf class, initializes the <see cref="IConnectionPool"/> object
        /// </summary>
        /// <returns></returns>
        protected abstract IConnectionPool InitializeConnectionPool();

    }
}