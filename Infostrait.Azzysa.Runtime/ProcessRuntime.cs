﻿using System;
using System.Net;
using Infostrait.Azzysa.Interfaces;

namespace Infostrait.Azzysa.Runtime
{

    /// <summary>
    /// Runtime to use when running in a Windows (FormsApp, ConsoleApp, ServiceApp etc.) context.
    /// </summary>
    public class ProcessRuntime : BaseRuntime
    {

        // Private member
        private CookieContainer _cookieContainer;

        /// <summary>
        /// Default constructor, initializes private members
        /// </summary>
        /// <param name="settingsProvider">Provide implementation to override base class implementation of initializing the <see cref="ISettingsProvider"/> object</param>
        /// <param name="connectionString"></param>
        /// <param name="serverName">Set the name of the azzysa server</param>
        /// <param name="siteName">Set the name of the site (where this server is member of)</param>
        /// <param name="rootPath">Sets the azzysa platform root directory (used for determine persistent storage, temp storary and application contents)</param>
        /// <exception cref="System.ArgumentNullException">When nog settings provider is provided</exception>
        public ProcessRuntime(ISettingsProvider settingsProvider, string connectionString, string serverName, string siteName, string rootPath) : base(settingsProvider, connectionString, serverName, siteName, rootPath)
        {
        }

        /// <summary>
        /// Returns the CookieContainer object
        /// </summary>
        public override CookieContainer CookieContainer => _cookieContainer;

        /// <summary>
        /// Not implemented
        /// </summary>
        /// <param name="connectionLock"></param>
        /// <returns></returns>
        public override IExchangeFrameworkProvider GetExchangeFrameworkProvider(IConnectionLock connectionLock)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Reset the cookie container by re-initializing the member
        /// </summary>
        public override void ResetCookieContainer()
        {
            // Initialize connection pool
            _cookieContainer = new CookieContainer();
        }

        /// <summary>
        /// No connection pool for ProcessRuntime
        /// </summary>
        public override IConnectionPool ConnectionPool => null;

        /// <summary>
        /// Initializes ProcessRuntime, set default CookieContainer
        /// </summary>
        public override void Initialize()
        {
            // Initialize connection pool
            _cookieContainer = new CookieContainer();
        }

        /// <summary>
        /// Clean up private members
        /// </summary>
        public override void Dispose()
        {
            _cookieContainer = null;
        }
    }
}